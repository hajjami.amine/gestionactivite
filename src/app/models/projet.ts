export class Projet {

    public reference: string;
    public client: string;
    constructor(reference: string, client: string) {
        this.reference = reference;
        this.client = client;
    }
}

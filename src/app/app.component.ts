import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/observable';
import { AppState, selectAuthenticationState } from './store/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'GA';
  getState: Observable<any>;
  session = false;
  ngOnInit() {
    this.getState.subscribe((state) => {
      console.log(state);
    this.session = state.isAuthenticated;

    });
  }
  constructor(
    private store: Store<AppState>
  ) {
    this.getState = this.store.select(selectAuthenticationState);
  }

}

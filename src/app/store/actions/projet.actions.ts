import { Action } from '@ngrx/store';

export enum ProjetActionTypes {
    ADD = '[Projet] ADD',
}
export class ADD implements Action {
    readonly type = ProjetActionTypes.ADD;
    constructor(public payload: any) { }
}

export type ProjetActions =
  | ADD;

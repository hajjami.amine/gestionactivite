import { AuthenticationActionTypes, AuthenticationActions } from '../actions/authentication.actions';
import { User } from 'src/app/models/User';
import { Projet } from 'src/app/models/projet';



export interface State {
    isAuthenticated: boolean;
    user: User | null;
    projet: Projet | null;
    errorMessage: string | null;
  }

export const initialState: State = {
    isAuthenticated: null,
    user: null,
    projet: null,
    errorMessage: null
  };

export function reducer(state = initialState, action: AuthenticationActions): State {
  console.log('reducer');
    switch (action.type) {
      case AuthenticationActionTypes.LOGIN_SUCCESS: {
        return {
          ...state,
          isAuthenticated: true,
          errorMessage: null
        };
      }
      case AuthenticationActionTypes.LOGIN_FAILURE: {
        return {
          ...state,
          errorMessage: 'Wrong credentials.'
        };
      }
      case AuthenticationActionTypes.LOGOUT: {
        return initialState;
      }
      default: {
        return state;
      }
    }
  }

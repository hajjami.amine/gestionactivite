import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/observable';
import { Router } from '@angular/router';
import { ProjetActionTypes, ADD } from '../actions/projet.actions';
import { ProjetService } from 'src/app/projets/projet.service';

@Injectable()
export class ProjetEffects {
    constructor(private actions$: Actions,
        private http: HttpClient, private projetServ: ProjetService) { }
    @Effect()
    ADD: Observable<any> = this.actions$.pipe(
        ofType(ProjetActionTypes.ADD),
        map((action: ADD) => action.payload),
        switchMap(payload => {
            return this.projetServ.addProject(payload.reference, payload.client);
        }
        )
    );
}

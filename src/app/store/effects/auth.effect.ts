import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/observable';
import { AuthenticationActionTypes, Login, LoginSuccess, LoginFailure } from '../actions/authentication.actions';
import { AuthService } from '../../authentification/authentification.service';
import { Router } from '@angular/router';
@Injectable()
export class AuthEffects {

    constructor(private actions$: Actions,
        private http: HttpClient, private authService: AuthService, private router: Router) { }

    @Effect()
    LogIn: Observable<any> = this.actions$.pipe(
        ofType(AuthenticationActionTypes.LOGIN),
        map((action: Login) => action.payload),
        switchMap(payload => {
            console.log('Effects');
            return this.authService.logIn(payload.email, payload.password).pipe(
                map((user) => {
                    if (user) {
                        return new LoginSuccess({ token: user.token, email: payload.email });
                    }
                    return new LoginFailure({ token: user.token, email: payload.email });
                }));
        })
    );

    @Effect({ dispatch: false })
    LogInSuccess: Observable<any> = this.actions$.pipe(
        ofType(AuthenticationActionTypes.LOGIN_SUCCESS),
        tap(() => {
            this.router.navigateByUrl('/');
        })
    );
    @Effect({ dispatch: false })
    LoginFailure: Observable<any> = this.actions$.pipe(
        ofType(AuthenticationActionTypes.LOGIN_FAILURE),
        tap(() => {
            this.router.navigateByUrl('/');
        })
    );

}

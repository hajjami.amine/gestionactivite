import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { TempsComponent } from './temps/temps.component';
import { FraisComponent } from './frais/frais.component';
import { AbsencesComponent } from './absences/absences.component';
import { ProjetsComponent } from './projets/projets.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { reducers } from './store/app.state';
import { AuthEffects } from './store/effects/auth.effect';
import {EffectsModule} from '@ngrx/effects';
import { ProjetEffects } from './store/effects/projet.effects';

const appRoutes: Routes = [
  { path: 'app-temps', component: TempsComponent },
  { path: 'app-frais', component: FraisComponent },
  { path: 'app-absences', component: AbsencesComponent },
  { path: 'app-projets', component: ProjetsComponent },
  { path: 'app-home', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    TempsComponent,
    FraisComponent,
    AbsencesComponent,
    ProjetsComponent,
    HomeComponent,
    AuthentificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([AuthEffects, ProjetEffects]),
    RouterModule.forRoot(
      appRoutes,
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

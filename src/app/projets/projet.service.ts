import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Projet } from '../models/projet';

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  private url = 'http://localhost:8080';
constructor(private http: HttpClient) { }

public getProjets(): Observable<Projet[]> {
    const url = 'http://localhost:8080/getAllProjets';
    return this.http.get<Projet[]>(url);
}
public addProject(reference: String, client: String) {
  const url = `${this.url}/addProject`;
  console.log({ reference, client });
  return this.http.post<Projet>(this.url, { reference, client });
}
}

import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { NG_PROJECT_AS_ATTR_NAME } from '@angular/core/src/render3/interfaces/projection';
import { Projet } from '../models/projet';
import { ProjetService } from './projet.service';
import { AppState, selectAuthenticationState } from '../store/app.state';
import { Store } from '@ngrx/store';
import { ADD } from '../store/actions/projet.actions';

@Component({
  selector: 'app-projets',
  templateUrl: './projets.component.html',
  styleUrls: ['./projets.component.css']
})
export class ProjetsComponent implements OnInit {
  projet: Projet = new Projet(null, null);
  projets = new Array<Projet>();
  getState: Observable<any>;
  constructor(projetService: ProjetService, private store: Store<AppState>) {
    projetService.getProjets().subscribe(
      response => {
        this.projets = response.map(item => {
          return new Projet(
            item.reference,
            item.client
          );
        });
      }
    );
    this.getState = this.store.select(selectAuthenticationState);
  }

  ngOnInit() {
  }
  onSubmit(): void {
    console.log('CREER');
    const payload = {
      reference: this.projet.reference,
      client: this.projet.client
    };
    this.store.dispatch(new ADD(payload));
  }
}

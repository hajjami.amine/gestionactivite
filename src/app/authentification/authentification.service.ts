import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private url = 'http://localhost:8080';
    constructor(private http: HttpClient) { }

    logIn(email: string, password: string): Observable<any> {
        const url = `${this.url}/authValidation`;
        console.log({ email, password });
        return this.http.post<boolean>(url, { email, password });
    }
}

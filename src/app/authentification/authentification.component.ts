import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/observable';
import { Login } from '../store/actions/authentication.actions';
import { AppState, selectAuthenticationState } from '../store/app.state';
@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {
  user: User = new User();
  constructor(private store: Store<AppState>) {
    this.getState = this.store.select(selectAuthenticationState);
  }
  getState: Observable<any>;
  ngOnInit() {
  }
  onSubmit(): void {
    console.log('submit');
    const payload = {
      email: this.user.email,
      password: this.user.password
    };
    this.store.dispatch(new Login(payload));
  }
}
